# Use an official Node runtime as a parent image
FROM node:14

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the package.json and package-lock.json files
COPY package*.json ./

# Install any dependencies
RUN npm install

# Bundle your app's source code inside the Docker image
COPY . .

# Your app binds to port 2023, so use the EXPOSE instruction to have it mapped by the docker daemon
EXPOSE 2023

# Define the command to run your app
CMD [ "node", "main.js" ]

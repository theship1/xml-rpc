const express = require('express');
const axios = require('axios');
const xmlrpc = require('xmlrpc');
const WebSocket = require('ws');
const api = require('api');
const app = express();
app.use(express.json());
 
const PORT = 2023;
 
 
//Artemis
const receiveDataFromXMLRPC = async () => {
    const client = xmlrpc.createClient({ host: '192.168.0.3', port: 2024, path: '/RPC2' });
 
    return new Promise((resolve, reject) => {
        client.methodCall('receive', [], (error, value) => {
            if (error) {
                console.error('Fehler beim Empfangen:', error);
                reject(error);
            } else {
                console.log('Empfangene Daten:', JSON.stringify(value, null, 2));
                resolve(value);
            }
        });
    });
};
 
const sendDataToXMLRPC = async (source, dataToSend) => {
    const client = xmlrpc.createClient({ host: '192.168.0.3', port: 2024, path: '/RPC2' });
 
    return new Promise((resolve, reject) => {
        client.methodCall('send', [source, Buffer.from(dataToSend)], (error, value) => {
            if (error) {
                console.error('Fehler beim Senden:', error);
                reject(error);
            } else {
                console.log('Antwort von send:', value);
                resolve(value);
            }
        });
    });
};
 


//Shangris

const receiveDataFromWS = () => {
    return new Promise((resolve, reject) => {
        const ws = new WebSocket('ws://192.168.0.3:2025/WS');

        ws.on('open', function open() {
            console.log('Connected to the server');
            // Optionally send a request to the server
            // ws.send('request data');
        });

        ws.on('message', function incoming(data) {
            console.log('Received data:', data);
            try {
                const parsedData = JSON.parse(data);
                console.log('Parsed data:', JSON.stringify(parsedData, null, 2));
                resolve(parsedData);
            } catch (error) {
                console.error('Error parsing received data:', error);
                reject(error);
            }
            ws.close();
        });

        ws.on('error', function error(err) {
            console.error('Connection error:', err);
            reject(err);
        });

        ws.on('close', function close() {
            console.log('Connection closed');
        });
    });
};

const sendDataToWS = (source, dataToSend) => {
    return new Promise((resolve, reject) => {
        const ws = new WebSocket('ws://192.168.0.3:2025/WS');

        ws.on('open', function open() {
            console.log('Connected to the server');
            const message = JSON.stringify({ source, data: Buffer.from(dataToSend).toString('base64') });
            ws.send(message);
        });

        ws.on('message', function incoming(data) {
            console.log('Response from server:', data);
            try {
                const parsedResponse = JSON.parse(data);
                console.log('Parsed response:', JSON.stringify(parsedResponse, null, 2));
                resolve(parsedResponse);
            } catch (error) {
                console.error('Error parsing response:', error);
                reject(error);
            }
            ws.close();
        });

        ws.on('error', function error(err) {
            console.error('Connection error:', err);
            reject(err);
        });

        ws.on('close', function close() {
            console.log('Connection closed');
        });
    });
};
 
 
 
//Elyse
const receiveDataFromAPI = () => {
    return new Promise((resolve, reject) => {
        const ws = new WebSocket('ws://192.168.0.3:2026/API');

        ws.on('open', function open() {
            console.log('Connected to the server');
            // Optionally send a message to the server after connecting
            // ws.send('Hello Server');
        });

        ws.on('message', function incoming(data) {
            console.log('Received data:', data);
            resolve(data);
            ws.close();
        });

        ws.on('error', function error(err) {
            console.error('Connection error:', err);
            reject(err);
        });

        ws.on('close', function close() {
            console.log('Connection closed');
        });
    });
};


const sendDataToAPI = (source, dataToSend) => {
    return new Promise((resolve, reject) => {
        const ws = new WebSocket('ws://192.168.0.3:2026/API');

        ws.on('open', function open() {
            console.log('Connected to the server');
            const message = JSON.stringify({ source, data: Buffer.from(dataToSend).toString('base64') });
            ws.send(message);
        });

        ws.on('message', function incoming(data) {
            console.log('Response from server:', data);
            resolve(data);
            ws.close();
        });

        ws.on('error', function error(err) {
            console.error('Connection error:', err);
            reject(err);
        });

        ws.on('close', function close() {
            console.log('Connection closed');
        });
    });
};
 
// für jede station seperat Endpints
 
//Artemis Station
app.post('/Artemis%20Station/receive', async (req, res) => {// auf das höhrt er und wenn gehöhrt, dann wird folgendes drin aufgerufen
    try{
        const receivedData = await receiveDataFromXMLRPC();//Get for Artemis
 
       
        res.json({ kind: "success", data: receivedData });
        }
     catch (error) {
    res.status(500).json({ kind: "error", message: error.message });
     }
});
 
 
app.post('/Artemis%20Station/send', async (req, res) => {
    try {
 
        await sendDataToXMLRPC(source, data);
        res.json({ kind: "success" });
    } catch (error) {
        res.status(500).json({ kind: "error", message: error.message });
    }
});
 
 
//Shangris
app.post('/Shangris%20Station/receive', async (req, res) => {// auf das höhrt er und wenn gehöhrt, dann wird folgendes drin aufgerufen
    try{
        const receivedData = await receiveDataFromWS();//Get for Shangris
 
       
        res.json({ kind: "success", data: receivedData });
        }
     catch (error) {
    res.status(500).json({ kind: "error", message: error.message });
     }
});
 
 
app.post('/Shangris%20Station/send', async (req, res) => {
    try {
 
        await sendDataToWS(source, data);
        res.json({ kind: "success" });
    } catch (error) {
        res.status(500).json({ kind: "error", message: error.message });
    }
});
 
 
 
 
//Elyse
app.post('/Elyse%20Terminal/receive', async (req, res) => {// auf das höhrt er und wenn gehöhrt, dann wird folgendes drin aufgerufen
    try{
        const receivedData = await receiveDataFromAPI();//Get for Elyse
 
       
        res.json({ kind: "success", data: receivedData });
        }
     catch (error) {
    res.status(500).json({ kind: "error", message: error.message });
     }
});
 
 
app.post('/Elyse%20Terminal/send', async (req, res) => {
    try {
 
        await sendDataToAPI(source, data);
        res.json({ kind: "success" });
    } catch (error) {
        res.status(500).json({ kind: "error", message: error.message });
    }
});
 
 
 
app.listen(PORT, () => {
    console.log(`Server läuft auf Port ${PORT}`);
});
 
//Dockerimage